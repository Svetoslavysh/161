﻿namespace Calculat
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.cos = new System.Windows.Forms.Button();
            this.numberOfSquare = new System.Windows.Forms.Button();
            this.sin = new System.Windows.Forms.Button();
            this.PowOfTwo = new System.Windows.Forms.Button();
            this.sum = new System.Windows.Forms.Button();
            this.answer = new System.Windows.Forms.TextBox();
            this.secondLine = new System.Windows.Forms.TextBox();
            this.firstLine = new System.Windows.Forms.TextBox();
            this.division = new System.Windows.Forms.Button();
            this.multiply = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.Negative = new System.Windows.Forms.Button();
            this.Tan = new System.Windows.Forms.Button();
            this.sumMod = new System.Windows.Forms.Button();
            this.subMod = new System.Windows.Forms.Button();
            this.CosG = new System.Windows.Forms.Button();
            this.SinG = new System.Windows.Forms.Button();
            this.Abs = new System.Windows.Forms.Button();
            this.Degree = new System.Windows.Forms.Button();
            this.LogarithmBack = new System.Windows.Forms.Button();
            this.Logarithm = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cos
            // 
            this.cos.Location = new System.Drawing.Point(312, 123);
            this.cos.Name = "cos";
            this.cos.Size = new System.Drawing.Size(53, 41);
            this.cos.TabIndex = 22;
            this.cos.Text = "cos ( a )";
            this.cos.UseVisualStyleBackColor = true;
            this.cos.Click += new System.EventHandler(this.operationOneArgument);
            // 
            // numberOfSquare
            // 
            this.numberOfSquare.Location = new System.Drawing.Point(312, 65);
            this.numberOfSquare.Name = "numberOfSquare";
            this.numberOfSquare.Size = new System.Drawing.Size(53, 41);
            this.numberOfSquare.TabIndex = 21;
            this.numberOfSquare.Text = "a ^ 2";
            this.numberOfSquare.UseVisualStyleBackColor = true;
            this.numberOfSquare.Click += new System.EventHandler(this.operationOneArgument);
            // 
            // sin
            // 
            this.sin.Location = new System.Drawing.Point(243, 123);
            this.sin.Name = "sin";
            this.sin.Size = new System.Drawing.Size(53, 41);
            this.sin.TabIndex = 20;
            this.sin.Text = "sin ( a )";
            this.sin.UseVisualStyleBackColor = true;
            this.sin.Click += new System.EventHandler(this.operationOneArgument);
            // 
            // PowOfTwo
            // 
            this.PowOfTwo.Location = new System.Drawing.Point(243, 65);
            this.PowOfTwo.Name = "PowOfTwo";
            this.PowOfTwo.Size = new System.Drawing.Size(53, 41);
            this.PowOfTwo.TabIndex = 19;
            this.PowOfTwo.Text = "2 ^ a";
            this.PowOfTwo.UseVisualStyleBackColor = true;
            this.PowOfTwo.Click += new System.EventHandler(this.operationOneArgument);
            // 
            // sum
            // 
            this.sum.Location = new System.Drawing.Point(22, 65);
            this.sum.Name = "sum";
            this.sum.Size = new System.Drawing.Size(54, 41);
            this.sum.TabIndex = 18;
            this.sum.Text = "a + b";
            this.sum.UseVisualStyleBackColor = true;
            this.sum.Click += new System.EventHandler(this.computation);
            // 
            // answer
            // 
            this.answer.Location = new System.Drawing.Point(22, 233);
            this.answer.Name = "answer";
            this.answer.Size = new System.Drawing.Size(409, 20);
            this.answer.TabIndex = 17;
            // 
            // secondLine
            // 
            this.secondLine.Location = new System.Drawing.Point(243, 28);
            this.secondLine.Name = "secondLine";
            this.secondLine.Size = new System.Drawing.Size(188, 20);
            this.secondLine.TabIndex = 16;
            // 
            // firstLine
            // 
            this.firstLine.Location = new System.Drawing.Point(22, 28);
            this.firstLine.Name = "firstLine";
            this.firstLine.Size = new System.Drawing.Size(187, 20);
            this.firstLine.TabIndex = 15;
            // 
            // division
            // 
            this.division.Location = new System.Drawing.Point(90, 123);
            this.division.Name = "division";
            this.division.Size = new System.Drawing.Size(53, 41);
            this.division.TabIndex = 14;
            this.division.Text = "a / b";
            this.division.UseVisualStyleBackColor = true;
            this.division.Click += new System.EventHandler(this.computation);
            // 
            // multiply
            // 
            this.multiply.Location = new System.Drawing.Point(22, 123);
            this.multiply.Name = "multiply";
            this.multiply.Size = new System.Drawing.Size(53, 41);
            this.multiply.TabIndex = 13;
            this.multiply.Text = "a * b";
            this.multiply.UseVisualStyleBackColor = true;
            this.multiply.Click += new System.EventHandler(this.computation);
            // 
            // minus
            // 
            this.minus.Location = new System.Drawing.Point(90, 65);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(53, 41);
            this.minus.TabIndex = 12;
            this.minus.Text = "a - b";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.computation);
            // 
            // Negative
            // 
            this.Negative.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Negative.Location = new System.Drawing.Point(378, 65);
            this.Negative.Name = "Negative";
            this.Negative.Size = new System.Drawing.Size(53, 41);
            this.Negative.TabIndex = 23;
            this.Negative.Text = "1/a";
            this.Negative.UseVisualStyleBackColor = false;
            this.Negative.Click += new System.EventHandler(this.operationOneArgument);
            // 
            // Tan
            // 
            this.Tan.Location = new System.Drawing.Point(378, 123);
            this.Tan.Name = "Tan";
            this.Tan.Size = new System.Drawing.Size(53, 41);
            this.Tan.TabIndex = 24;
            this.Tan.Text = "tan ( a )";
            this.Tan.UseVisualStyleBackColor = true;
            this.Tan.Click += new System.EventHandler(this.operationOneArgument);
            // 
            // sumMod
            // 
            this.sumMod.Location = new System.Drawing.Point(156, 65);
            this.sumMod.Name = "sumMod";
            this.sumMod.Size = new System.Drawing.Size(53, 41);
            this.sumMod.TabIndex = 25;
            this.sumMod.Text = "| a + b |";
            this.sumMod.UseVisualStyleBackColor = true;
            this.sumMod.Click += new System.EventHandler(this.computation);
            // 
            // subMod
            // 
            this.subMod.Location = new System.Drawing.Point(156, 123);
            this.subMod.Name = "subMod";
            this.subMod.Size = new System.Drawing.Size(53, 41);
            this.subMod.TabIndex = 26;
            this.subMod.Text = "| a - b |";
            this.subMod.UseVisualStyleBackColor = true;
            this.subMod.Click += new System.EventHandler(this.computation);
            // 
            // CosG
            // 
            this.CosG.Location = new System.Drawing.Point(378, 177);
            this.CosG.Name = "CosG";
            this.CosG.Size = new System.Drawing.Size(53, 41);
            this.CosG.TabIndex = 27;
            this.CosG.Text = "cos\'( a )";
            this.CosG.UseVisualStyleBackColor = true;
            this.CosG.Click += new System.EventHandler(this.operationOneArgument);
            // 
            // SinG
            // 
            this.SinG.Location = new System.Drawing.Point(312, 177);
            this.SinG.Name = "SinG";
            this.SinG.Size = new System.Drawing.Size(53, 41);
            this.SinG.TabIndex = 28;
            this.SinG.Text = "sin\'( a )";
            this.SinG.UseVisualStyleBackColor = true;
            this.SinG.Click += new System.EventHandler(this.operationOneArgument);
            // 
            // Abs
            // 
            this.Abs.Location = new System.Drawing.Point(243, 177);
            this.Abs.Name = "Abs";
            this.Abs.Size = new System.Drawing.Size(53, 41);
            this.Abs.TabIndex = 29;
            this.Abs.Text = "| a |";
            this.Abs.UseVisualStyleBackColor = true;
            this.Abs.Click += new System.EventHandler(this.operationOneArgument);
            // 
            // Degree
            // 
            this.Degree.Location = new System.Drawing.Point(156, 177);
            this.Degree.Name = "Degree";
            this.Degree.Size = new System.Drawing.Size(53, 41);
            this.Degree.TabIndex = 30;
            this.Degree.Text = "a ^ b";
            this.Degree.UseVisualStyleBackColor = true;
            this.Degree.Click += new System.EventHandler(this.computation);
            // 
            // LogarithmBack
            // 
            this.LogarithmBack.ForeColor = System.Drawing.SystemColors.ControlText;
            this.LogarithmBack.Location = new System.Drawing.Point(90, 177);
            this.LogarithmBack.Name = "LogarithmBack";
            this.LogarithmBack.Size = new System.Drawing.Size(53, 41);
            this.LogarithmBack.TabIndex = 31;
            this.LogarithmBack.Text = "Logb(a)";
            this.LogarithmBack.UseVisualStyleBackColor = true;
            this.LogarithmBack.Click += new System.EventHandler(this.computation);
            // 
            // Logarithm
            // 
            this.Logarithm.Location = new System.Drawing.Point(22, 177);
            this.Logarithm.Name = "Logarithm";
            this.Logarithm.Size = new System.Drawing.Size(53, 41);
            this.Logarithm.TabIndex = 32;
            this.Logarithm.Text = "Loga(b)";
            this.Logarithm.UseVisualStyleBackColor = true;
            this.Logarithm.Click += new System.EventHandler(this.computation);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 279);
            this.Controls.Add(this.Logarithm);
            this.Controls.Add(this.LogarithmBack);
            this.Controls.Add(this.Degree);
            this.Controls.Add(this.Abs);
            this.Controls.Add(this.SinG);
            this.Controls.Add(this.CosG);
            this.Controls.Add(this.subMod);
            this.Controls.Add(this.sumMod);
            this.Controls.Add(this.Tan);
            this.Controls.Add(this.Negative);
            this.Controls.Add(this.cos);
            this.Controls.Add(this.numberOfSquare);
            this.Controls.Add(this.sin);
            this.Controls.Add(this.PowOfTwo);
            this.Controls.Add(this.sum);
            this.Controls.Add(this.answer);
            this.Controls.Add(this.secondLine);
            this.Controls.Add(this.firstLine);
            this.Controls.Add(this.division);
            this.Controls.Add(this.multiply);
            this.Controls.Add(this.minus);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cos;
        private System.Windows.Forms.Button numberOfSquare;
        private System.Windows.Forms.Button sin;
        private System.Windows.Forms.Button PowOfTwo;
        private System.Windows.Forms.Button sum;
        private System.Windows.Forms.TextBox answer;
        private System.Windows.Forms.TextBox secondLine;
        private System.Windows.Forms.TextBox firstLine;
        private System.Windows.Forms.Button division;
        private System.Windows.Forms.Button multiply;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button Negative;
        private System.Windows.Forms.Button Tan;
        private System.Windows.Forms.Button sumMod;
        private System.Windows.Forms.Button subMod;
        private System.Windows.Forms.Button CosG;
        private System.Windows.Forms.Button SinG;
        private System.Windows.Forms.Button Abs;
        private System.Windows.Forms.Button Degree;
        private System.Windows.Forms.Button LogarithmBack;
        private System.Windows.Forms.Button Logarithm;
    }
}

