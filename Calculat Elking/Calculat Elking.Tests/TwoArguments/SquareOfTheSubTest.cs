﻿using Calculat_Elking.TwoArguments;
using NUnit.Framework;

namespace Calculat_Elking.Tests.TwoArguments
{
    public class SquareOfTheSubTest
    {
        [TestCase(5, 3, 4)]
        [TestCase(10, 9, 1)]
        [TestCase(34, 30, 16)]
        public void CalculateTest(double firstValue, double secondValue, double expected)
        {
            var calculator = new SquareOfTheSub();
            var actualResult = calculator.Calculate(firstValue, secondValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
