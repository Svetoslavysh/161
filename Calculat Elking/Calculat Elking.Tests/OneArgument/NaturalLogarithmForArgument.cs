﻿using Calculat_Elking.OneArgument;
using NUnit.Framework;

namespace Calculat_Elking.Tests.OneArgument
{
    class NaturalLogarithmForArgument
    {
        [TestCase(1, 0)]
        [TestCase(1, 0)]
        [TestCase(1, 0)]
        public void CalculateTest(double firstValue, double expected)
        {
            var calculator = new NaturalLofarithmForArgument();
            var actualResult = calculator.Calculate(firstValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
