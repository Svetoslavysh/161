﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Calculat.TwoArgument;

namespace CalculatTest.TwoArgument
{
    public class LogarithmBackTest
    {

        [TestCase(1, 2, 0)]
        [TestCase(16, 2, 4)]
        [TestCase(49, 7, 2)]
        public void CalculateTest(double firstValue, double secondValue, double expected)
        {
            var calculator = new LogarithmBack();
            var actualResult = calculator.Calculate(firstValue, secondValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
