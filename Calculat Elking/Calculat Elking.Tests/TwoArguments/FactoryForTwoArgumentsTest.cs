﻿using System;
using Calculat_Elking.TwoArguments;
using NUnit.Framework;

namespace Calculat_Elking.Tests.TwoArguments
{
    public class TestFactory
    {
        [TestCase("sum", typeof(Addition))]
        [TestCase("minus", typeof(Subtraction))]
        [TestCase("multiply", typeof(Multiply))]
        [TestCase("division", typeof(Division))]
        [TestCase("argumentAndItsDegree", typeof(ArgumentAndItsDegree))]
        [TestCase("argumentAndItsInverseDegree", typeof(ArgumentAndItsInverseDegree))]
        [TestCase("squareOfTheSum", typeof(SquareOfTheSum))]
        [TestCase("squareOfTheSub", typeof(SquareOfTheSub))]
        [TestCase("sumModulo", typeof(SumModulo))]
        [TestCase("subModulo", typeof(SubModulo))]
        public void CalculateTest(string name, Type type)
        {
            var calculator = Factory.CreateCalculation(name);

            Assert.IsInstanceOf(type, calculator);
        }

    }
}