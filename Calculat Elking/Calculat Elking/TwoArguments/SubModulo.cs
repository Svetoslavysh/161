﻿using System;

namespace Calculat_Elking.TwoArguments
{
    /// <summary>
    /// Calculation of the module for subtraction of two numbers
    /// </summary>
    public class SubModulo : IAction
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="firstArgument">The first argument for computing</param>
        /// <param name="secondArgument">The second argument for computing</param>
        /// <returns>Return of the module of subtraction of two numbers</returns>
        public double Calculate(double firstArgument, double secondArgument)
        {
            return Math.Abs(firstArgument - secondArgument);
        }
    }
}
