﻿namespace Calculat.TwoArgument
{
    /// <summary>
    /// Calculating the subtraction of two numbers
    /// </summary>
    public class Subtraction : ITwoArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="firstArgument">The first argument for computing</param>
        /// <param name="secondArgument">The second argument for computing</param>
        /// <returns>Return subtraction of two numbers</returns>
        public double Calculate(double firstArgument, double secondArgument)
        {
            return firstArgument - secondArgument;
        }
    }
}
