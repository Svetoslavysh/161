﻿namespace Calculat.OneArgument
{
    /// <summary>
    /// Interface for working with one argument
    /// </summary>
    public interface IOneArgument
    {
        double Calculate(double firstArgument);
    }
}
