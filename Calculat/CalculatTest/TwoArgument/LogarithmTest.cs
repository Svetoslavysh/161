﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Calculat.TwoArgument;

namespace CalculatTest.TwoArgument
{
    public class LogarithmTest
    {

        [TestCase(2, 4, 2)]
        [TestCase(2, 16, 4)]
        [TestCase(7, 49, 2)]
        public void CalculateTest(double firstValue, double secondValue, double expected)
        {
            var calculator = new Logarithm();
            var actualResult = calculator.Calculate(firstValue, secondValue);
            Assert.AreEqual(expected, actualResult);
        }

    }
}
