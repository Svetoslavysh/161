﻿using Calculat_Elking.TwoArguments;
using NUnit.Framework;
using System;

namespace Calculat_Elking.Tests.TwoArguments
{
    [TestFixture]
    class ArgumentAndItsInverseDegreeErrorTest
    {
        [TestCase(4, 2, 2)]

        public void ArgumentAnsItsInverseDegreeErrorTests(double firstArgument, double secondArgument, double expected)
        {
            ArgumentAndItsInverseDegree calculator = new ArgumentAndItsInverseDegree();
            double result = calculator.Calculate(firstArgument, secondArgument);
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void DivisionByZero()
        {
            ArgumentAndItsInverseDegree calculator = new ArgumentAndItsInverseDegree();
            Assert.Throws<Exception>(() => calculator.Calculate(4, 0));
        }
    }
}
