﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculat.OneArgument
{
    /// <summary>
    /// Calculating the minus first degree of the argument
    /// </summary>
    public class Negative : IOneArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="firstArgument">Operated number</param>
        /// <returns>Return the minus first degree of the argument</returns>
        public double Calculate(double firstArgument)
        {
            return 1 / firstArgument;
        }
    }
}
