﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Calculat.TwoArgument;

namespace CalculatTest.TwoArgument
{
    public class SubtracktionModTest
    {
        [TestCase(0, 0, 0)]
        [TestCase(7, 4, 3)]
        [TestCase(-7, 2, 9)]
        public void CalculateTest(double firstValue, double secondValue, double expected)
        {
            var calculator = new SubtractionMod();
            var actualResult = calculator.Calculate(firstValue, secondValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
