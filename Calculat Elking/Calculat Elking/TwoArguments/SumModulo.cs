﻿using System;

namespace Calculat_Elking.TwoArguments
{
    /// <summary>
    /// Calculation of the module for adding two numbers
    /// </summary>
    public class SumModulo : IAction
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="firstArgument">The first argument for computing</param>
        /// <param name="secondArgument">The second argument for computing</param>
        /// <returns>Return a module to add two numbers</returns>
        public double Calculate(double firstArgument, double secondArgument)
        {
            return Math.Abs(firstArgument + secondArgument);
        }
    }
}
