﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculat.TwoArgument
{
    /// <summary>
    /// Calculation of the logarithm of the second argument at base first argument
    /// </summary> 
    public class Logarithm : ITwoArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="firstArgument">Operated number</param>
        /// <param name="secondArgument">Operated number</param>
        /// <returns>Returning the logarithm of a number</returns>
        public double Calculate(double firstArgument, double secondArgument)
        {
            if ( (secondArgument == 1) || (secondArgument <= 0) )
            {
                throw new Exception("Ошибка. Основание логарифма не можнт быть равно 1");
            }
            return Math.Log(secondArgument, firstArgument);
        }
    }
}
