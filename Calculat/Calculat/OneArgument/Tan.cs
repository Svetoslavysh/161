﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculat.OneArgument
{
    /// <summary>
    /// Calculating the sine
    /// </summary>
    public class Tan : IOneArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="firstArgument">Operated number</param>
        /// <returns>Return of the tan of the argument</returns>
        public double Calculate(double firstArgument)
        {
            return Math.Tan(firstArgument);
        }
    }
}
