﻿using Calculat_Elking.TwoArguments;
using NUnit.Framework;

namespace Calculat_Elking.Tests.TwoArguments
{
    public class SubModuloTest
    {
        [TestCase(1, 2, 1)]
        [TestCase(4, 2, 2)]
        [TestCase(3, 27, 24)]
        public void CalculateTest(double firstValue, double secondValue, double expected)
        {
            var calculator = new SubModulo();
            var actualResult = calculator.Calculate(firstValue, secondValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
