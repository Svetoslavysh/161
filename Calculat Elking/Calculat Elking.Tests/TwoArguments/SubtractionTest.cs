﻿using NUnit.Framework;
using Calculat_Elking.TwoArguments;

namespace Calculat_Elking.Tests.TwoArguments
{
    class SubtractionTest
    {
        [TestCase (7, 4, 3) ]
        [TestCase (3, 4, -1) ]
        [TestCase (-7, -2, -5) ]
        public void CalculateTest(double firstArgument, double secondArgument, double expected)
        {
            var calculator = new Subtraction();
            var actualResult = calculator.Calculate(firstArgument, secondArgument);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
