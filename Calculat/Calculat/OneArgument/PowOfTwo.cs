﻿using System;

namespace Calculat.OneArgument
{
    /// <summary>
    /// Calculating the two in the power of the argument
    /// </summary>
    public class PowOfTwo : IOneArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="firstArgument">Operated number</param>
        /// <returns>Returning the two to the power of the argument</returns>
        public double Calculate(double firstArgument)
        {
            return Math.Pow(2, firstArgument);
        }
    }
}
