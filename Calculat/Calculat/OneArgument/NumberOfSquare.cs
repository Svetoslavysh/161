﻿using System;

namespace Calculat.OneArgument
{
    /// <summary>
    /// Calculating the square of the argument
    /// </summary>
    public class NumberOfSquare : IOneArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="firstArgument">Operated number</param>
        /// <returns>Return the square of the argument</returns>
        public double Calculate(double firstArgument)
        {
            return Math.Pow(firstArgument, 2);
        }
    }
}
