﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Calculat.TwoArgument;

namespace CalculatTest.TwoArgument
{
    public class FactoryTest
    {
        [TestCase("sum", typeof(Addition))]
        [TestCase("minus", typeof(Subtraction))]
        [TestCase("multiply", typeof(Multiply))]
        [TestCase("division", typeof(Division))]
        [TestCase("sumMod", typeof(AdditionMod))]
        [TestCase("subMod", typeof(SubtractionMod))]
        [TestCase("Logarithm", typeof(Logarithm))]
        [TestCase("LogarithmBack", typeof(LogarithmBack))]
        [TestCase("Degree", typeof(Degree))]
        public void CalculateTest(string name, Type type)
        {
            var calculator = Factory.CreateCalculation(name);

            Assert.IsInstanceOf(type, calculator);
        }

    }
}
