﻿using Calculat_Elking.TwoArguments;
using NUnit.Framework;

namespace Calculat_Elking.Tests.TwoArguments
{
    public class SumModuloTest
    {
        [TestCase(7, 9, 16)]
        [TestCase(4, 2, 6)]
        [TestCase(2, 30, 32)]
        public void CalculateTest(double firstValue, double secondValue, double expected)
        {
            var calculator = new SumModulo();
            var actualResult = calculator.Calculate(firstValue, secondValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
