﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Calculat.OneArgument;

namespace CalculatTest.OneArgument
{
    public class CosTest
    {
        [TestCase(0, 1)]
        [TestCase(5, 0.28)]
        [TestCase(-1, 0.54)]
        public void CalculateTest(double firstValue, double expected)
        {
            var calculator = new Cos();
            var actualResult = calculator.Calculate(firstValue);
            Assert.AreEqual(expected, actualResult, 0.01);
        }
    }
}
