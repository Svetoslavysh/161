﻿using NUnit.Framework;
using Calculat_Elking.OneArgument;

namespace Calculat_Elking.Tests.OneArgument
{
    public class PowerOfExpanentTest
    {
        [TestCase(0, 1)]
        [TestCase(0, 1)]
        [TestCase(0, 1)]
        public void CalculateTest(double firstValue, double expected)
        {
            var calculator = new PowerOfExpanent();
            var actualResult = calculator.Calculate(firstValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
