﻿using System;

namespace Calculat.TwoArgument

{
    /// <summary>
    /// Calculating the division of two numbers
    /// </summary>
    public class Division : ITwoArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="firstArgument">The first argument for computing</param>
        /// <param name="secondArgument">The second argument for computing</param>
        /// <returns>Returning the division of two numbers</returns>
        public double Calculate(double firstArgument, double secondArgument)
        {
            if (firstArgument == 0 || secondArgument == 0)
            {
                throw new Exception("Ошибка. Нет смысла делить ноль на число или число на ноль.");
            }
            return firstArgument / secondArgument;
        }
    }
}
