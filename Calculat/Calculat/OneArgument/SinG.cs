﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculat.OneArgument
{
    /// <summary>
    /// To calculate the sine in degrees
    /// </summary>
    public class SinG : IOneArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="firstArgument">Operated number</param>
        /// <returns>Return the sine in degrees of the argument</returns>
        public double Calculate(double firstArgument)
        {
            return Math.Sin(firstArgument * Math.PI / 180);
        }
    }
}
