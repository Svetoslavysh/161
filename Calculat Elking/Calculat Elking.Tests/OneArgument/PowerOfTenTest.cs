﻿using NUnit.Framework;
using Calculat_Elking.OneArgument;

namespace Calculat_Elking.Tests.OneArgument
{
    public class PowerOfTenTest
    {
        [TestCase(2, 100)]
        [TestCase(0, 1)]
        [TestCase(1, 10)]
        public void CalculateTest(double firstValue, double expected)
        {
            var calculator = new PowerOfTen();
            var actualResult = calculator.Calculate(firstValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
