﻿using Calculat_Elking.TwoArguments;
using NUnit.Framework;

namespace Calculat_Elking.Tests.TwoArguments
{
    public class ArgumentAndItsDegreeTest
    {
        [TestCase(10, 2, 100)]
        [TestCase(3, 2, 9)]
        [TestCase(-7, 2, 49)]
        public void CalculateTest(double firstValue, double secondValue, double expected)
        {
            var calculator = new ArgumentAndItsDegree();
            var actualResult = calculator.Calculate(firstValue, secondValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
