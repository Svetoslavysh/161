﻿using System;

namespace Calculat_Elking.OneArgument
{
    /// <summary>
    /// Calculation of exponent in degree
    /// </summary>
    public class PowerOfExpanent : IOperationForOneArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="Argument">Operated number</param>
        /// <returns>Return of the exponent to the power of the argument</returns>
        public double Calculate(double Argument)
        {
            return Math.Exp(Argument);
        }
    }
}
