﻿using System;

namespace Calculat_Elking.TwoArguments
{
    /// <summary>
    /// Calculating an argument to the power of another argument
    /// </summary>
    public class ArgumentAndItsDegree : IAction
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="firstArgument">The first argument for computing</param>
        /// <param name="secondArgument">The second argument for computing</param>
        /// <returns>Returning an argument to the power of another argument</returns>
        public double Calculate(double firstArgument, double secondArgument)
        {
            return Math.Pow(firstArgument,secondArgument);
        }
    }
}
