﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculat.TwoArgument
{
    /// <summary>
    /// Calculation of the logarithm of the first argument at base second argument
    /// </summary> 
    public class LogarithmBack : ITwoArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="firstArgument">Operated number</param>
        /// <param name="secondArgument">Operated number</param>
        /// <returns>Returning the logarithm of a number</returns>
        public double Calculate(double firstArgument, double secondArgument)
        {
            return Math.Log(firstArgument, secondArgument);
        }
    }
}
