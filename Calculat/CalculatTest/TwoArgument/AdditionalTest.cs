﻿using NUnit.Framework;
using Calculat.TwoArgument;

namespace CalculatTest.TwoArgument
{
   
    public class AdditionalTest
    {
        [TestCase(0, 0, 0)]
        [TestCase(3, 4, 7)]
        [TestCase(-7, -2, -9)]
        public void CalculateTest(double firstValue, double secondValue, double expected)
        {
            var calculator = new Addition();
            var actualResult = calculator.Calculate(firstValue, secondValue);
            Assert.AreEqual(expected, actualResult);
        }

    }

}
