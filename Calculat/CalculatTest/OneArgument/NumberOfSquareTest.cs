﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Calculat.OneArgument;

namespace CalculatTest.OneArgument
{
    public class NumberOfSquareTest
    {

        [TestCase(2, 4)]
        [TestCase(5, 25)]
        [TestCase(10, 100)]
        public void CalculateTest(double firstValue, double expected)
        {
            var calculator = new NumberOfSquare();
            var actualResult = calculator.Calculate(firstValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
