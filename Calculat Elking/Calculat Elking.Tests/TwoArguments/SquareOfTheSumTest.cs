﻿using Calculat_Elking.TwoArguments;
using NUnit.Framework;

namespace Calculat_Elking.Tests.TwoArguments
{
    public class SquareOfTheSumTest
    {
        [TestCase(1, 2, 9)]
        [TestCase(4, 2, 36)]
        [TestCase(27, 3, 900)]
        public void CalculateTest(double firstValue, double secondValue, double expected)
        {
            var calculator = new SquareOfTheSum();
            var actualResult = calculator.Calculate(firstValue, secondValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
