﻿using Calculat_Elking.OneArgument;
using NUnit.Framework;


namespace Calculat_Elking.Tests.OneArgument
{
    public class NumberInSquareTestClass1
    {
        [TestCase(12, 144)]
        [TestCase(0, 0)]
        [TestCase(5, 25)]
        public void CalculateTest(double firstValue, double expected)
        {
            var calculator = new NumberInSquare();
            var actualResult = calculator.Calculate(firstValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
