﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculat
{
    class Factory
    {
        static public Interface CreateCalculation(string name)
        {
            switch (name)
            {
                case "sum":
                    return new Addition();
                case "minus":
                    return new Subtraction();
                case "multiply":
                    return new Multiply();
                case "division":
                    return new Division();
                default:
                    throw new Exception(" Неизвестная операция ");
            }
        }
    }
}
