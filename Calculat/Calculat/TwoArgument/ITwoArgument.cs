﻿namespace Calculat.TwoArgument
{
    /// <summary>
    /// Interface for working with two arguments
    /// </summary>
    public interface ITwoArgument
    {
        double Calculate(double firstArgument, double secondArgument);
    }
}
