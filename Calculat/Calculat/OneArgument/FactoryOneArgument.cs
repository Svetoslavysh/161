﻿using System;

namespace Calculat.OneArgument
{
    /// <summary>
    /// The interface class for one argument
    /// </summary>
    public class FactoryOneArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="name">Name of the incoming command</param>
        /// <returns>Create a new calculator with the desired action</returns>
        public static IOneArgument CreateCalculation(string name)
        {
            switch (name)
            {
                case "PowOfTwo":
                    return new PowOfTwo();
                case "numberOfSquare":
                    return new NumberOfSquare();
                case "sin":
                    return new Sin();
                case "cos":
                    return new Cos();
                case "Negative":
                    return new Negative();
                case "Tan":
                    return new Tan();
                case "Abs":
                    return new Abs();
                case "SinG":
                    return new SinG();
                case "CosG":
                    return new CosG();
                default:
                    throw new Exception(" Неизвестная операция ");
            }
        }
    }
}
