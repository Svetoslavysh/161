﻿using System;

namespace Calculat.OneArgument
{
    /// <summary>
    /// Calculating the sine
    /// </summary>
    public class Sin : IOneArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="firstArgument">Operated number</param>
        /// <returns>Return of the sine of the argument</returns>
        public double Calculate(double firstArgument)
        {
            return Math.Sin(firstArgument);
        }
    }
}
