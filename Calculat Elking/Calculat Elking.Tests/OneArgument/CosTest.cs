﻿using Calculat_Elking.OneArgument;
using NUnit.Framework;

namespace Calculat_Elking.Tests.OneArgument
{
    class CosTest
    {
        [TestCase(0, 1)]
        [TestCase(0, 1)]
        [TestCase(0, 1)]
        public void CalculateTest(double firstValue, double expected)
        {
            var calculator = new Cos();
            var actualResult = calculator.Calculate(firstValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
