﻿namespace Calculat_Elking.OneArgument
{
    /// <summary>
    /// Interface for working with one argument
    /// </summary>
    public interface IOperationForOneArgument
    {
        double Calculate(double Argument);
    }
}
