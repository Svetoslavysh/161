﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Calculat.OneArgument;


namespace CalculatTest.OneArgument
{
    public class SinGTest
    {
        [TestCase(30, 0.50)]
        [TestCase(0, 0)]
        [TestCase(60, 0.86)]
        public void CalculateTest(double firstValue, double expected)
        {
            var calculator = new SinG();
            var actualResult = calculator.Calculate(firstValue);
            Assert.AreEqual(expected, actualResult, 0.01);
        }
    }
}
