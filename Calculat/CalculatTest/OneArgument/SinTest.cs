﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Calculat.OneArgument;

namespace CalculatTest.OneArgument
{
    class SinTest
    {
        [TestCase(0, 0)]
        [TestCase(5, -0.95)]
        [TestCase(-1, -0.84)]
        public void CalculateTest(double firstValue, double expected)
        {
            var calculator = new Sin();
            var actualResult = calculator.Calculate(firstValue);
            Assert.AreEqual(expected, actualResult, 0.01);
        }
    }
}
