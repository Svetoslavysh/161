﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculat.TwoArgument
{
    /// <summary>
    /// Calculation of the module for subtraction of two numbers
    /// </summary>
    public class SubtractionMod : ITwoArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="firstArgument">The first argument for computing</param>
        /// <param name="secondArgument">The second argument for computing</param>
        /// <returns>Return of the module of subtraction of two numbers</returns>
        public double Calculate(double firstArgument, double secondArgument)
        {
            return Math.Abs(firstArgument - secondArgument);
        }
    }
}
