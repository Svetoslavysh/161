﻿namespace Calculat_Elking.TwoArguments
{
    /// <summary>
    /// Interface for working with two arguments
    /// </summary>
    public interface IAction
    {
        double Calculate(double firstArgument, double secondArgument);
    }
}
