﻿using System;

namespace Calculat_Elking.OneArgument
{
    /// <summary>
    /// Calculating the sine
    /// </summary>
    public class Sin : IOperationForOneArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="Argument">Operated number</param>
        /// <returns>Return of the sine of the argument</returns>
        public double Calculate(double Argument)
        {
            return Math.Sin(Argument);
        }
    }
}
