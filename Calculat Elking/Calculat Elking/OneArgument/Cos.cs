﻿using System;

namespace Calculat_Elking.OneArgument
{
    /// <summary>
    /// To calculate the cosine
    /// </summary>
    public class Cos : IOperationForOneArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="Argument">Operated number</param>
        /// <returns>Return the cosine of the argument</returns>
        public double Calculate(double Argument)
        {
            return Math.Cos(Argument);
        }
    }
}
