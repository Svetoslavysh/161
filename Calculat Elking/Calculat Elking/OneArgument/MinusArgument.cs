﻿namespace Calculat_Elking.OneArgument
{
    /// <summary>
    /// Finding a negative argument
    /// </summary>
    public class MinuseArgument : IOperationForOneArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="Argument">Operated number</param>
        /// <returns>Returning a negative argument</returns>
        public double Calculate(double Argument)
        {
            return -Argument;
        }
    }
}
