﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Calculat.OneArgument;

namespace CalculatTest.OneArgument
{
    public class PowOfTwoTest
    {
        [TestCase(2, 4)]
        [TestCase(5, 32)]
        [TestCase(-1, 0.5)]
        public void CalculateTest(double firstValue, double expected)
        {
            var calculator = new PowOfTwo();
            var actualResult = calculator.Calculate(firstValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
