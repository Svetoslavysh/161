﻿using System;

namespace Calculat.OneArgument
{
    /// <summary>
    /// To calculate the cosine
    /// </summary>
    public class Cos : IOneArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="firstArgument">Operated number</param>
        /// <returns>Return the cosine of the argument</returns>
        public double Calculate(double firstArgument)
        {
            return Math.Cos(firstArgument);
        }
    }
}
