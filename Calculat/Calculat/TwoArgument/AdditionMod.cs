﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculat.TwoArgument
{
    /// <summary>
    /// Calculation of the module for adding two numbers
    /// </summary>
    public class AdditionMod : ITwoArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="firstArgument">The first argument for computing</param>
        /// <param name="secondArgument">The second argument for computing</param>
        /// <returns>Return a module to add two numbers</returns>
        public double Calculate(double firstArgument, double secondArgument)
        {
            return Math.Abs(firstArgument + secondArgument);
        }
    }
}
