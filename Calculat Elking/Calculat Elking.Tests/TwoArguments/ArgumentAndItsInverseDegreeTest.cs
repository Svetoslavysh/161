﻿using Calculat_Elking.TwoArguments;
using NUnit.Framework;

namespace Calculat_Elking.Tests.TwoArguments
{
    public class ArgumentAndItsInverseDegreeTest
    {
        [TestCase(25, 2, 5)]
        [TestCase(4, 2, 2)]
        [TestCase(27, 3, 3)]
        public void CalculateTest(double firstValue, double secondValue, double expected)
        {
            var calculator = new ArgumentAndItsInverseDegree();
            var actualResult = calculator.Calculate(firstValue, secondValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
