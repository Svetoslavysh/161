﻿using System;

namespace Calculat_Elking.TwoArguments
{
    /// <summary>
    /// Calculating the square of the subtraction of two numbers
    /// </summary>
    public class SquareOfTheSub : IAction
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="firstArgument">The first argument for computing</param>
        /// <param name="secondArgument">The second argument for computing</param>
        /// <returns>Return the square of the subtraction of two numbers</returns>
        public double Calculate(double firstArgument, double secondArgument)
        {
            return Math.Pow((firstArgument-secondArgument),2);
        }
    }
}
