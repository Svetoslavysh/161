﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Calculat.OneArgument;

namespace CalculatTest.OneArgument
{
    public class TanTest
    {
        [TestCase(0, 0)]
        [TestCase(1, 1.55)]
        [TestCase(-1, -1.55)]
        public void CalculateTest(double firstValue, double expected)
        {
            var calculator = new Tan();
            var actualResult = calculator.Calculate(firstValue);
            Assert.AreEqual(expected, actualResult, 0.01);
        }
    }
}
