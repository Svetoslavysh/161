﻿namespace Calculat_Elking.TwoArguments
{
    /// <summary>
    /// Calculating the multiplication of two arguments
    /// </summary>
    public class Multiply : IAction
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="firstArgument">The first argument for computing</param>
        /// <param name="secondArgument">The second argument for computing</param>
        /// <returns>Returning the multiplication of two arguments</returns>
        public double Calculate(double firstArgument, double secondArgument)
        {
            return firstArgument * secondArgument;
        }
    }
}
