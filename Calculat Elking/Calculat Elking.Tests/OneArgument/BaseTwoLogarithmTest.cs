﻿using Calculat_Elking.OneArgument;
using NUnit.Framework;

namespace Calculat_Elking.Tests.OneArgument
{
    class BaseTwoLogarithmTest
    {
        [TestCase(2, 1)]
        [TestCase(4, 2)]
        [TestCase(8, 3)]
        public void CalculateTest(double firstValue, double expected)
        {
            var calculator = new BaseTwoLogarithm();
            var actualResult = calculator.Calculate(firstValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
