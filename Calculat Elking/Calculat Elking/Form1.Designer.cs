﻿namespace Calculat_Elking
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.minus = new System.Windows.Forms.Button();
            this.multiply = new System.Windows.Forms.Button();
            this.division = new System.Windows.Forms.Button();
            this.firstLine = new System.Windows.Forms.TextBox();
            this.secondLine = new System.Windows.Forms.TextBox();
            this.sum = new System.Windows.Forms.Button();
            this.powerOfTwo = new System.Windows.Forms.Button();
            this.sin = new System.Windows.Forms.Button();
            this.numberInSquare = new System.Windows.Forms.Button();
            this.cos = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.answer = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.powerOfExpanent = new System.Windows.Forms.Button();
            this.naturalLogarifmForArgument = new System.Windows.Forms.Button();
            this.log2 = new System.Windows.Forms.Button();
            this.log10 = new System.Windows.Forms.Button();
            this.powerOfTen = new System.Windows.Forms.Button();
            this.minusArgument = new System.Windows.Forms.Button();
            this.argumentAndItsInverseDegree = new System.Windows.Forms.Button();
            this.sumModulo = new System.Windows.Forms.Button();
            this.subModulo = new System.Windows.Forms.Button();
            this.squareOfTheSum = new System.Windows.Forms.Button();
            this.argumentAndItsDegree = new System.Windows.Forms.Button();
            this.squareOfTheSub = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // minus
            // 
            this.minus.BackColor = System.Drawing.SystemColors.Control;
            this.minus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.minus.ForeColor = System.Drawing.SystemColors.ControlText;
            this.minus.Location = new System.Drawing.Point(249, 88);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(122, 33);
            this.minus.TabIndex = 0;
            this.minus.Text = "a - b";
            this.minus.UseVisualStyleBackColor = false;
            this.minus.Click += new System.EventHandler(this.computation);
            // 
            // multiply
            // 
            this.multiply.BackColor = System.Drawing.SystemColors.Control;
            this.multiply.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.multiply.Location = new System.Drawing.Point(249, 127);
            this.multiply.Name = "multiply";
            this.multiply.Size = new System.Drawing.Size(122, 33);
            this.multiply.TabIndex = 2;
            this.multiply.Text = "a * b";
            this.multiply.UseVisualStyleBackColor = false;
            this.multiply.Click += new System.EventHandler(this.computation);
            // 
            // division
            // 
            this.division.BackColor = System.Drawing.SystemColors.Control;
            this.division.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.division.Location = new System.Drawing.Point(249, 166);
            this.division.Name = "division";
            this.division.Size = new System.Drawing.Size(122, 33);
            this.division.TabIndex = 3;
            this.division.Text = "a / b";
            this.division.UseVisualStyleBackColor = false;
            this.division.Click += new System.EventHandler(this.computation);
            // 
            // firstLine
            // 
            this.firstLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.firstLine.Location = new System.Drawing.Point(44, 51);
            this.firstLine.Name = "firstLine";
            this.firstLine.Size = new System.Drawing.Size(184, 23);
            this.firstLine.TabIndex = 4;
            // 
            // secondLine
            // 
            this.secondLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.secondLine.Location = new System.Drawing.Point(44, 82);
            this.secondLine.Name = "secondLine";
            this.secondLine.Size = new System.Drawing.Size(184, 23);
            this.secondLine.TabIndex = 5;
            // 
            // sum
            // 
            this.sum.BackColor = System.Drawing.SystemColors.Control;
            this.sum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.sum.ForeColor = System.Drawing.SystemColors.ControlText;
            this.sum.Location = new System.Drawing.Point(249, 49);
            this.sum.Name = "sum";
            this.sum.Size = new System.Drawing.Size(122, 33);
            this.sum.TabIndex = 7;
            this.sum.Text = "a + b";
            this.sum.UseVisualStyleBackColor = false;
            this.sum.Click += new System.EventHandler(this.computation);
            // 
            // powerOfTwo
            // 
            this.powerOfTwo.BackColor = System.Drawing.SystemColors.Control;
            this.powerOfTwo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.powerOfTwo.Location = new System.Drawing.Point(541, 88);
            this.powerOfTwo.Name = "powerOfTwo";
            this.powerOfTwo.Size = new System.Drawing.Size(122, 33);
            this.powerOfTwo.TabIndex = 8;
            this.powerOfTwo.Text = "2 ^ a";
            this.powerOfTwo.UseVisualStyleBackColor = false;
            this.powerOfTwo.Click += new System.EventHandler(this.operationForOneOperation);
            // 
            // sin
            // 
            this.sin.BackColor = System.Drawing.SystemColors.Control;
            this.sin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.sin.Location = new System.Drawing.Point(541, 167);
            this.sin.Name = "sin";
            this.sin.Size = new System.Drawing.Size(122, 32);
            this.sin.TabIndex = 9;
            this.sin.Text = "sin ( a )";
            this.sin.UseVisualStyleBackColor = false;
            this.sin.Click += new System.EventHandler(this.operationForOneOperation);
            // 
            // numberInSquare
            // 
            this.numberInSquare.BackColor = System.Drawing.SystemColors.Control;
            this.numberInSquare.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.numberInSquare.Location = new System.Drawing.Point(541, 49);
            this.numberInSquare.Name = "numberInSquare";
            this.numberInSquare.Size = new System.Drawing.Size(122, 31);
            this.numberInSquare.TabIndex = 10;
            this.numberInSquare.Text = "a ^ 2";
            this.numberInSquare.UseVisualStyleBackColor = false;
            this.numberInSquare.Click += new System.EventHandler(this.operationForOneOperation);
            // 
            // cos
            // 
            this.cos.BackColor = System.Drawing.SystemColors.Control;
            this.cos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.cos.Location = new System.Drawing.Point(541, 204);
            this.cos.Name = "cos";
            this.cos.Size = new System.Drawing.Size(122, 32);
            this.cos.TabIndex = 11;
            this.cos.Text = "cos ( a )";
            this.cos.UseVisualStyleBackColor = false;
            this.cos.Click += new System.EventHandler(this.operationForOneOperation);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(7, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 20);
            this.label1.TabIndex = 12;
            this.label1.Text = "a =";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.Location = new System.Drawing.Point(7, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 20);
            this.label2.TabIndex = 13;
            this.label2.Text = "b =";
            // 
            // answer
            // 
            this.answer.AutoSize = true;
            this.answer.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.answer.Location = new System.Drawing.Point(97, 252);
            this.answer.Name = "answer";
            this.answer.Size = new System.Drawing.Size(0, 25);
            this.answer.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label3.Location = new System.Drawing.Point(401, -2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(197, 24);
            this.label3.TabIndex = 15;
            this.label3.Text = "Выберите действие:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label4.Location = new System.Drawing.Point(259, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(228, 17);
            this.label4.TabIndex = 16;
            this.label4.Text = "Для двух аргументов ( \' a \' и \' b \' )";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label5.Location = new System.Drawing.Point(550, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(241, 17);
            this.label5.TabIndex = 17;
            this.label5.Text = "Для одного аргумента (только \' a \')";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.label6.Location = new System.Drawing.Point(12, 251);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 26);
            this.label6.TabIndex = 18;
            this.label6.Text = "Ответ:";
            // 
            // powerOfExpanent
            // 
            this.powerOfExpanent.BackColor = System.Drawing.SystemColors.Control;
            this.powerOfExpanent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.powerOfExpanent.ForeColor = System.Drawing.SystemColors.ControlText;
            this.powerOfExpanent.Location = new System.Drawing.Point(669, 204);
            this.powerOfExpanent.Name = "powerOfExpanent";
            this.powerOfExpanent.Size = new System.Drawing.Size(122, 32);
            this.powerOfExpanent.TabIndex = 19;
            this.powerOfExpanent.Text = "e ^ a";
            this.powerOfExpanent.UseVisualStyleBackColor = false;
            this.powerOfExpanent.Click += new System.EventHandler(this.operationForOneOperation);
            // 
            // naturalLogarifmForArgument
            // 
            this.naturalLogarifmForArgument.BackColor = System.Drawing.SystemColors.Control;
            this.naturalLogarifmForArgument.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.naturalLogarifmForArgument.ForeColor = System.Drawing.SystemColors.ControlText;
            this.naturalLogarifmForArgument.Location = new System.Drawing.Point(669, 130);
            this.naturalLogarifmForArgument.Name = "naturalLogarifmForArgument";
            this.naturalLogarifmForArgument.Size = new System.Drawing.Size(122, 32);
            this.naturalLogarifmForArgument.TabIndex = 20;
            this.naturalLogarifmForArgument.Text = "ln ( a )";
            this.naturalLogarifmForArgument.UseVisualStyleBackColor = false;
            this.naturalLogarifmForArgument.Click += new System.EventHandler(this.operationForOneOperation);
            // 
            // log2
            // 
            this.log2.BackColor = System.Drawing.SystemColors.Control;
            this.log2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.log2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.log2.Location = new System.Drawing.Point(669, 88);
            this.log2.Name = "log2";
            this.log2.Size = new System.Drawing.Size(122, 33);
            this.log2.TabIndex = 21;
            this.log2.Text = "log 2 ( a )";
            this.log2.UseVisualStyleBackColor = false;
            this.log2.Click += new System.EventHandler(this.operationForOneOperation);
            // 
            // log10
            // 
            this.log10.BackColor = System.Drawing.SystemColors.Control;
            this.log10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.log10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.log10.Location = new System.Drawing.Point(669, 49);
            this.log10.Name = "log10";
            this.log10.Size = new System.Drawing.Size(122, 31);
            this.log10.TabIndex = 22;
            this.log10.Text = "log 10 ( a )";
            this.log10.UseVisualStyleBackColor = false;
            this.log10.Click += new System.EventHandler(this.operationForOneOperation);
            // 
            // powerOfTen
            // 
            this.powerOfTen.BackColor = System.Drawing.SystemColors.Control;
            this.powerOfTen.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.powerOfTen.ForeColor = System.Drawing.SystemColors.ControlText;
            this.powerOfTen.Location = new System.Drawing.Point(541, 130);
            this.powerOfTen.Name = "powerOfTen";
            this.powerOfTen.Size = new System.Drawing.Size(122, 30);
            this.powerOfTen.TabIndex = 23;
            this.powerOfTen.Text = "10 ^ a";
            this.powerOfTen.UseVisualStyleBackColor = false;
            this.powerOfTen.Click += new System.EventHandler(this.operationForOneOperation);
            // 
            // minusArgument
            // 
            this.minusArgument.BackColor = System.Drawing.SystemColors.Control;
            this.minusArgument.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.minusArgument.ForeColor = System.Drawing.SystemColors.ControlText;
            this.minusArgument.Location = new System.Drawing.Point(669, 167);
            this.minusArgument.Name = "minusArgument";
            this.minusArgument.Size = new System.Drawing.Size(122, 32);
            this.minusArgument.TabIndex = 24;
            this.minusArgument.Text = "- a";
            this.minusArgument.UseVisualStyleBackColor = false;
            this.minusArgument.Click += new System.EventHandler(this.operationForOneOperation);
            // 
            // argumentAndItsInverseDegree
            // 
            this.argumentAndItsInverseDegree.BackColor = System.Drawing.SystemColors.Control;
            this.argumentAndItsInverseDegree.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.argumentAndItsInverseDegree.Location = new System.Drawing.Point(377, 49);
            this.argumentAndItsInverseDegree.Name = "argumentAndItsInverseDegree";
            this.argumentAndItsInverseDegree.Size = new System.Drawing.Size(122, 33);
            this.argumentAndItsInverseDegree.TabIndex = 25;
            this.argumentAndItsInverseDegree.Text = "a ^ 1/b";
            this.argumentAndItsInverseDegree.UseVisualStyleBackColor = false;
            this.argumentAndItsInverseDegree.Click += new System.EventHandler(this.computation);
            // 
            // sumModulo
            // 
            this.sumModulo.BackColor = System.Drawing.SystemColors.Control;
            this.sumModulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.sumModulo.Location = new System.Drawing.Point(377, 88);
            this.sumModulo.Name = "sumModulo";
            this.sumModulo.Size = new System.Drawing.Size(122, 33);
            this.sumModulo.TabIndex = 26;
            this.sumModulo.Text = "| a + b |";
            this.sumModulo.UseVisualStyleBackColor = false;
            this.sumModulo.Click += new System.EventHandler(this.computation);
            // 
            // subModulo
            // 
            this.subModulo.BackColor = System.Drawing.SystemColors.Control;
            this.subModulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.subModulo.Location = new System.Drawing.Point(377, 127);
            this.subModulo.Name = "subModulo";
            this.subModulo.Size = new System.Drawing.Size(122, 33);
            this.subModulo.TabIndex = 27;
            this.subModulo.Text = "| a - b |";
            this.subModulo.UseVisualStyleBackColor = false;
            this.subModulo.Click += new System.EventHandler(this.computation);
            // 
            // squareOfTheSum
            // 
            this.squareOfTheSum.BackColor = System.Drawing.SystemColors.Control;
            this.squareOfTheSum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.squareOfTheSum.Location = new System.Drawing.Point(377, 166);
            this.squareOfTheSum.Name = "squareOfTheSum";
            this.squareOfTheSum.Size = new System.Drawing.Size(122, 33);
            this.squareOfTheSum.TabIndex = 28;
            this.squareOfTheSum.Text = "( a + b )^2";
            this.squareOfTheSum.UseVisualStyleBackColor = false;
            this.squareOfTheSum.Click += new System.EventHandler(this.computation);
            // 
            // argumentAndItsDegree
            // 
            this.argumentAndItsDegree.BackColor = System.Drawing.SystemColors.Control;
            this.argumentAndItsDegree.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.argumentAndItsDegree.Location = new System.Drawing.Point(249, 205);
            this.argumentAndItsDegree.Name = "argumentAndItsDegree";
            this.argumentAndItsDegree.Size = new System.Drawing.Size(122, 33);
            this.argumentAndItsDegree.TabIndex = 29;
            this.argumentAndItsDegree.Text = "a ^ b";
            this.argumentAndItsDegree.UseVisualStyleBackColor = false;
            this.argumentAndItsDegree.Click += new System.EventHandler(this.computation);
            // 
            // squareOfTheSub
            // 
            this.squareOfTheSub.BackColor = System.Drawing.SystemColors.Control;
            this.squareOfTheSub.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.squareOfTheSub.Location = new System.Drawing.Point(377, 205);
            this.squareOfTheSub.Name = "squareOfTheSub";
            this.squareOfTheSub.Size = new System.Drawing.Size(122, 33);
            this.squareOfTheSub.TabIndex = 30;
            this.squareOfTheSub.Text = "( a - b )^2";
            this.squareOfTheSub.UseVisualStyleBackColor = false;
            this.squareOfTheSub.Click += new System.EventHandler(this.computation);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(812, 292);
            this.Controls.Add(this.squareOfTheSub);
            this.Controls.Add(this.argumentAndItsDegree);
            this.Controls.Add(this.squareOfTheSum);
            this.Controls.Add(this.subModulo);
            this.Controls.Add(this.sumModulo);
            this.Controls.Add(this.argumentAndItsInverseDegree);
            this.Controls.Add(this.minusArgument);
            this.Controls.Add(this.powerOfTen);
            this.Controls.Add(this.log10);
            this.Controls.Add(this.log2);
            this.Controls.Add(this.naturalLogarifmForArgument);
            this.Controls.Add(this.powerOfExpanent);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.answer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cos);
            this.Controls.Add(this.numberInSquare);
            this.Controls.Add(this.sin);
            this.Controls.Add(this.powerOfTwo);
            this.Controls.Add(this.sum);
            this.Controls.Add(this.secondLine);
            this.Controls.Add(this.firstLine);
            this.Controls.Add(this.division);
            this.Controls.Add(this.multiply);
            this.Controls.Add(this.minus);
            this.Name = "Form1";
            this.Text = "Калькулятор";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button multiply;
        private System.Windows.Forms.Button division;
        private System.Windows.Forms.TextBox firstLine;
        private System.Windows.Forms.TextBox secondLine;
        private System.Windows.Forms.Button sum;
        private System.Windows.Forms.Button powerOfTwo;
        private System.Windows.Forms.Button sin;
        private System.Windows.Forms.Button numberInSquare;
        private System.Windows.Forms.Button cos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label answer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button powerOfExpanent;
        private System.Windows.Forms.Button naturalLogarifmForArgument;
        private System.Windows.Forms.Button log2;
        private System.Windows.Forms.Button log10;
        private System.Windows.Forms.Button powerOfTen;
        private System.Windows.Forms.Button minusArgument;
        private System.Windows.Forms.Button argumentAndItsInverseDegree;
        private System.Windows.Forms.Button sumModulo;
        private System.Windows.Forms.Button subModulo;
        private System.Windows.Forms.Button squareOfTheSum;
        private System.Windows.Forms.Button argumentAndItsDegree;
        private System.Windows.Forms.Button squareOfTheSub;
    }
}

