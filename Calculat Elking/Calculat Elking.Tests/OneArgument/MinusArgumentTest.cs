﻿using Calculat_Elking.OneArgument;
using NUnit.Framework;

namespace Calculat_Elking.Tests.OneArgument
{
    public class MinusArgumentTest
    {
        [TestCase(12, -12)]
        [TestCase(0, 0)]
        [TestCase(5, -5)]
        public void CalculateTest(double firstValue, double expected)
        {
            var calculator = new MinuseArgument();
            var actualResult = calculator.Calculate(firstValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
