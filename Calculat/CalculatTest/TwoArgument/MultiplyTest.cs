﻿using Calculat.TwoArgument;
using NUnit.Framework;

namespace CalculatTest.TwoArgument
{
    public class MultiplyTest
    {
        [TestCase(2, 3, 6)]
        [TestCase(3, 3, 9)]
        [TestCase(-7, -2, 14)]
        public void CalculateTest(double firstValue, double secondValue, double expected)
        {
            var calculator = new Multiply();
            var actualResult = calculator.Calculate(firstValue, secondValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
