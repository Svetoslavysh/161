﻿using System;
using NUnit.Framework;
using Calculat_Elking.OneArgument;

namespace Calculat_Elking.Tests.OneArgument
{
    [TestFixture]
    public class BaseTenLogarithmErrorTest
    {
        [TestCase(10, 1)]

        public void DivisionErrorTests(double firstArgument, double expected)
        {
            BaseTenLogarithm calculator = new BaseTenLogarithm();
            double result = calculator.Calculate(firstArgument);
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void LogarithmByZeroOrOne()
        {
            BaseTenLogarithm calculator = new BaseTenLogarithm();
            Assert.Throws<Exception>(() => calculator.Calculate(0));
        }
    }
}
