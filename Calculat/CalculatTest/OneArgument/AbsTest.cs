﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Calculat.OneArgument;

namespace CalculatTest.OneArgument
{
    public class AbsTest
    {
        [TestCase(1, 1)]
        [TestCase(-5, 5)]
        [TestCase(-1, 1)]
        public void CalculateTest(double firstValue, double expected)
        {
            var calculator = new Abs();
            var actualResult = calculator.Calculate(firstValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
