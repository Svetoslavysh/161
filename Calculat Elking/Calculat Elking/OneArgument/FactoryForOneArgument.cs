﻿using System;

namespace Calculat_Elking.OneArgument
{
    /// <summary>
    /// The interface class for one argument
    /// </summary>
    public class FactoryForOneArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="name">Name of the incoming command</param>
        /// <returns>Create a new calculator with the desired action</returns>
        static public IOperationForOneArgument CreateCalculation(string name)
        {
            switch (name)
            {
                case "powerOfTwo":
                    return new PowerOfTwo();
                case "numberInSquare":
                    return new NumberInSquare();
                case "sin":
                    return new Sin();
                case "cos":
                    return new Cos();
                case "powerOfTen":
                     return new PowerOfTen();
                case "powerOfExpanent":
                    return new PowerOfExpanent();
                case "minusArgument":
                    return new MinuseArgument();
                case "naturalLogarifmForArgument":
                    return new NaturalLofarithmForArgument();
                case "log2":
                    return new BaseTwoLogarithm();
                case "log10":
                    return new BaseTenLogarithm();
                default:
                    throw new Exception(" Неизвестная операция ");
            }
        }
    }
}
