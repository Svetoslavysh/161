﻿using System;

namespace Calculat_Elking.TwoArguments
{
    /// <summary>
    /// The interface class for two arguments
    /// </summary>
    public class Factory
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="name">Name of the incoming command</param>
        /// <returns>Create a new calculator with the desired action</returns>
        static public IAction CreateCalculation(string name)
        {
            switch (name)
            {
                case "sum":
                    return new Addition();
                case "minus":
                    return new Subtraction();
                case "multiply":
                    return new Multiply();
                case "division":
                    return new Division();
                case "argumentAndItsDegree":
                    return new ArgumentAndItsDegree();
                case "argumentAndItsInverseDegree":
                    return new ArgumentAndItsInverseDegree();
                case "squareOfTheSum":
                    return new SquareOfTheSum();
                case "squareOfTheSub":
                    return new SquareOfTheSub();
                case "sumModulo":
                    return new SumModulo();
                case "subModulo":
                    return new SubModulo();
                default:
                    throw new Exception(" Неизвестная операция ");
            }
        }
    }
}
