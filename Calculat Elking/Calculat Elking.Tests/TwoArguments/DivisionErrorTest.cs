﻿using System;
using NUnit.Framework;
using Calculat_Elking.TwoArguments;

namespace Calculat_Elking.Tests.TwoArguments
{
    [TestFixture]
    class DivisionErrorTest
    {
        [TestCase(50, 10, 5)]

        public void DivisionErrorTests(double firstArgument, double secondArgument, double expected)
        {
            Division calculator = new Division();
            double result = calculator.Calculate(firstArgument, secondArgument);
            Assert.AreEqual(expected, result, 0.0001);
        }

        [Test]
        public void DivisionByZero()
        {
            Division calculator = new Division();
            Assert.Throws<Exception>(() => calculator.Calculate(0,0));
        }
    }
}
