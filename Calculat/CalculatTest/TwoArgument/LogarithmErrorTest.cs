﻿using System;
using Calculat.TwoArgument;
using NUnit.Framework;

namespace CalculatTest.TwoArgument
{
    [TestFixture]
    class LogarithmErrorTest
    {
        [TestCase(2, 8, 3)]

        public void LogarithmErrorTests(double firstArgument, double secondArgument, double expected)
        {
            Logarithm calculator = new Logarithm();
            double result = calculator.Calculate(firstArgument, secondArgument);
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void LogarithmByZero()
        {
            Logarithm calculator = new Logarithm();
            Assert.Throws<Exception>(() => calculator.Calculate(2,0));
        }
    }
}
