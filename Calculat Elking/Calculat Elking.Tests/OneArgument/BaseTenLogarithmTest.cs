﻿using Calculat_Elking.OneArgument;
using NUnit.Framework;

namespace Calculat_Elking.Tests.OneArgument
{
    class BaseTenLogarithmTest
    {
        [TestCase(10, 1)]
        [TestCase(100, 2)]
        [TestCase(10, 1)]
        public void CalculateTest(double firstValue, double expected)
        {
            var calculator = new BaseTenLogarithm();
            var actualResult = calculator.Calculate(firstValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
