﻿using System;

namespace Calculat_Elking.OneArgument
{
    /// <summary>
    /// Calculation of the logarithm at base two
    /// </summary>
    public class BaseTwoLogarithm : IOperationForOneArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="Argument">Operated number</param>
        /// <returns>Returning the logarithm of a number</returns>
        public double Calculate(double Argument)
        {
            if ((Argument <= 0) || (Argument == 0))
            {
                throw new Exception("Ошибка. Логарифм существует только у положительных чисел.(Кроме единциы)");
            }
            return Math.Log(Argument,2);
        }
    }
}
