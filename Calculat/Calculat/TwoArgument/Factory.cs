﻿using System;

namespace Calculat.TwoArgument
{
    /// <summary>
    /// The interface class for two arguments
    /// </summary>
    public class Factory
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="name">Name of the incoming command</param>
        /// <returns>Create a new calculator with the desired action</returns>
        public static ITwoArgument CreateCalculation(string name)
        {
            switch (name)
            {
                case "sum":
                    return new Addition();
                case "minus":
                    return new Subtraction();
                case "multiply":
                    return new Multiply();
                case "division":
                    return new Division();
                case "sumMod":
                    return new AdditionMod();
                case "subMod":
                    return new SubtractionMod();
                case "Logarithm":
                    return new Logarithm();
                case "LogarithmBack":
                    return new LogarithmBack();
                case "Degree":
                    return new Degree();
                default:
                    throw new Exception(" Неизвестная операция ");
            }
        }
    }
}
