﻿using System;
using NUnit.Framework;
using Calculat_Elking.OneArgument;

namespace Calculat_Elking.Tests.OneArgument
{
    [TestFixture]
    class BaseTwoLogarithmErrorTest
    {
        [TestCase(2, 1)]

        public void DivisionErrorTests(double firstArgument, double expected)
        {
            BaseTwoLogarithm calculator = new BaseTwoLogarithm();
            double result = calculator.Calculate(firstArgument);
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void LogafithmByZeroOrOne()
        {
            BaseTwoLogarithm calculator = new BaseTwoLogarithm();
            Assert.Throws<Exception>(() => calculator.Calculate(0));
        }
    }
}
