﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Calculat.OneArgument;


namespace CalculatTest.OneArgument
{
     public class NegativeTest
    {
        [TestCase(1, 1)]
        [TestCase(2, 0.5)]
        [TestCase(10, 0.1)]
        public void CalculateTest(double firstValue, double expected)
        {
            var calculator = new Negative();
            var actualResult = calculator.Calculate(firstValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
