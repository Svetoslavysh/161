﻿using System;
using System.Windows.Forms;
using Calculat_Elking.OneArgument;
using Calculat_Elking.TwoArguments;

namespace Calculat_Elking
{ 
    public partial class Form1 : Form 
    { 
        public Form1()
        {
            InitializeComponent();
        }

        private void computation(object sender, EventArgs e)
        {
            try
            {
                double answer1;
                double firstArgument = Convert.ToDouble(firstLine.Text);
                double secondArgument = Convert.ToDouble(secondLine.Text);
                IAction calculator = Factory.CreateCalculation(((Button)sender).Name);
                answer1 = calculator.Calculate(firstArgument, secondArgument);
                answer.Text = Convert.ToString(answer1);
            }

            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }   
        }

        private void operationForOneOperation(object sender, EventArgs e)
        {
            
            try
            {
                double answer1;
                double argument = Convert.ToDouble(firstLine.Text);
                IOperationForOneArgument calculator = FactoryForOneArgument.CreateCalculation(((Button) sender).Name);
                answer1 = calculator.Calculate(argument);
                answer.Text = Convert.ToString(answer1);
            }

            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }

        }
    }
}
