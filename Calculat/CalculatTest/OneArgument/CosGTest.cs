﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Calculat.OneArgument;

namespace CalculatTest.OneArgument
{
    public class CosGTest
    {
        [TestCase(0, 1)]
        [TestCase(60, 0.5)]
        [TestCase(-1, 0.99)]
        public void CalculateTest(double firstValue, double expected)
        {
            var calculator = new CosG();
            var actualResult = calculator.Calculate(firstValue);
            Assert.AreEqual(expected, actualResult, 0.01);
        }
    }
}
