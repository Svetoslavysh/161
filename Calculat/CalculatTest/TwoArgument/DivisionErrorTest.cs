﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Calculat.TwoArgument;


namespace CalculatTest.TwoArgument
{
    [TestFixture]
    class DivisionErrorTest
    {
        [TestCase(50, 10, 5)]

        public void DivisionErrorTests(double firstArgument, double secondArgument, double expected)
        {
            Division calculator = new Division();
            double result = calculator.Calculate(firstArgument, secondArgument);
            Assert.AreEqual(expected, result, 0.0001);
        }

        [Test]
        public void DivisionByZero()
        {
            Division calculator = new Division();
            Assert.Throws<Exception>(() => calculator.Calculate(0, 0));
        }
    }
}
