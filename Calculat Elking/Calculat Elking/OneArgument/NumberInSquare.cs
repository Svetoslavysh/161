﻿using System;

namespace Calculat_Elking.OneArgument
{
    /// <summary>
    /// Calculating the square of the argument
    /// </summary>
    public class NumberInSquare : IOperationForOneArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="Argument">Operated number</param>
        /// <returns>Return the square of the argument</returns>
        public double Calculate(double Argument)
        {
            return Math.Pow(Argument, 2);
        }
    }
}
