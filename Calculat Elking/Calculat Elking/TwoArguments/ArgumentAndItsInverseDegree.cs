﻿using System;

namespace Calculat_Elking.TwoArguments
{
    /// <summary>
    /// Calculating the argument to the power of another inverse argument
    /// </summary>
    public class ArgumentAndItsInverseDegree : IAction
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="firstArgument">The first argument for computing</param>
        /// <param name="secondArgument">The second argument for computing</param>
        /// <returns>Returning an argument to the power of another inverse argument</returns>
        public double Calculate(double firstArgument, double secondArgument)
        {
            if (secondArgument == 0)
            {
                throw new Exception("Ошибка. В степени не может быть деление на ноль.");
            }
            return Math.Pow(firstArgument,1/secondArgument);
        }
    }
}
