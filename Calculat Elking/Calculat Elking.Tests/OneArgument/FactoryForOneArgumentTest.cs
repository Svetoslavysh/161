﻿using Calculat_Elking.OneArgument;
using NUnit.Framework;
using System;

namespace Calculat_Elking.Tests.OneArgument
{
    public class FactoryForOneArgumentTest
    {
        [TestCase("powerOfTwo", typeof(PowerOfTwo))]
        [TestCase("numberInSquare", typeof(NumberInSquare))]
        [TestCase("sin", typeof(Sin))]
        [TestCase("cos", typeof(Cos))]
        [TestCase("powerOfTen", typeof(PowerOfTen))]
        [TestCase("powerOfExpanent", typeof(PowerOfExpanent))]
        [TestCase("minusArgument", typeof(MinuseArgument))]
        [TestCase("naturalLogarifmForArgument", typeof(NaturalLofarithmForArgument))]
        [TestCase("log2", typeof(BaseTwoLogarithm))]
        [TestCase("log10", typeof(BaseTenLogarithm))]

        public void CalculateTest(string name, Type type)
        {
            var calculator = FactoryForOneArgument.CreateCalculation(name);

            Assert.IsInstanceOf(type, calculator);
        }
    }
}
