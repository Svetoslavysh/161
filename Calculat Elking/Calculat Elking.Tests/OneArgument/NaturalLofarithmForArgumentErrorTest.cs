﻿using Calculat_Elking.OneArgument;
using NUnit.Framework;
using System;

namespace Calculat_Elking.Tests.OneArgument
{
    class NaturalLofarithmForArgumentErrorTest
    {
        [TestCase(1, 0)]

        public void DivisionErrorTests(double firstArgument, double expected)
        {
            NaturalLofarithmForArgument calculator = new NaturalLofarithmForArgument();
            double result = calculator.Calculate(firstArgument);
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void LogafithmByZeroOrOne()
        {
            NaturalLofarithmForArgument calculator = new NaturalLofarithmForArgument();
            Assert.Throws<Exception>(() => calculator.Calculate(0));
        }
    }
}
