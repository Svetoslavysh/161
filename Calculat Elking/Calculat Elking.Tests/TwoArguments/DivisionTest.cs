﻿using Calculat_Elking.TwoArguments;
using NUnit.Framework;

namespace Calculat_Elking.Tests.TwoArguments
{
    class DivisionTest
    {
        [TestCase(4, 2, 2)]
        [TestCase(6, 3, 2)]
        [TestCase(-8, 2, -4)]
        public void CalculateTest(double firstValue, double secondValue, double expected)
        {
            var calculator = new Division();
            var actualResult = calculator.Calculate(firstValue, secondValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
