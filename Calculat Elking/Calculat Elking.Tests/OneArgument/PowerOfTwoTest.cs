﻿using NUnit.Framework;
using Calculat_Elking.OneArgument;

namespace Calculat_Elking.Tests.OneArgument
{
    public class PowerOfTwoTest
    {
        [TestCase(2, 4)]
        [TestCase(1, 2)]
        [TestCase(0, 1)]
        public void CalculateTest(double firstValue, double expected)
        {
            var calculator = new PowerOfTwo();
            var actualResult = calculator.Calculate(firstValue);
            Assert.AreEqual(expected, actualResult);
        }

    }
}
