﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Calculat.OneArgument;

namespace CalculatTest.OneArgument
{
    public class OneArgumentFactoryTest
    {
        [TestCase("PowOfTwo", typeof(PowOfTwo))]
        [TestCase("numberOfSquare", typeof(NumberOfSquare))]
        [TestCase("sin", typeof(Sin))]
        [TestCase("cos", typeof(Cos))]
        [TestCase("Negative", typeof(Negative))]
        [TestCase("Tan", typeof(Tan))]
        [TestCase("Abs", typeof(Abs))]
        [TestCase("SinG", typeof(SinG))]
        [TestCase("CosG", typeof(CosG))]
        public void CalculateTest(string name, Type type)
        {
            var calculator = FactoryOneArgument.CreateCalculation(name);

            Assert.IsInstanceOf(type, calculator);
        }
    }
}
