﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Calculat.TwoArgument;

namespace CalculatTest.TwoArgument
{
    public class DegreeTest
    {
        [TestCase(1, 0, 1)]
        [TestCase(2, 5, 32)]
        [TestCase(-7, 2, 49)]
        public void CalculateTest(double firstValue, double secondValue, double expected)
        {
            var calculator = new Degree();
            var actualResult = calculator.Calculate(firstValue, secondValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
