﻿using System;

namespace Calculat_Elking.OneArgument
{
    /// <summary>
    /// Calculating the tens to the power of the argument
    /// </summary>
    public class PowerOfTen : IOperationForOneArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="Argument">Operated number</param>
        /// <returns>Return of the tens to the power of the argument</returns>
        public double Calculate(double Argument)
        {
            return Math.Pow(10, Argument);
        }
    }
}
