﻿using System;

namespace Calculat_Elking.OneArgument
{
    /// <summary>
    /// Calculation of the natural logarithm
    /// </summary>
    public class NaturalLofarithmForArgument : IOperationForOneArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="Argument">Operated number</param>
        /// <returns>Returning the logarithm of a number</returns>
        public double Calculate(double Argument)
        {
            if ((Argument <= 0) || (Argument == 0))
            {
                throw new Exception("Ошибка. Логарифм существует только у положительных чисел.(Кроме единицы)");
            }
            return Math.Log10(Argument);
        }
    }
}
