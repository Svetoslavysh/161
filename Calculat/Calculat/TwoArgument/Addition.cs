﻿namespace Calculat.TwoArgument
{
    /// <summary>
    /// Class for adding two numbers
    /// </summary>
    public class Addition : ITwoArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="firstArgument">The first argument for computing</param>
        /// <param name="secondArgument">The second argument for computing</param>
        /// <returns>Return the sum of two numbers</returns>
        public double Calculate(double firstArgument, double secondArgument)
        {
            return firstArgument + secondArgument;
        }
    }
}
