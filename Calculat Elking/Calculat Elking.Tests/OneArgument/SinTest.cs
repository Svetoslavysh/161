﻿using NUnit.Framework;
using Calculat_Elking.OneArgument;

namespace Calculat_Elking.Tests.OneArgument
{
    public class SinTest
    {
        [TestCase(0, 0)]
        [TestCase(0, 0)]
        [TestCase(0, 0)]
        public void CalculateTest(double firstValue, double expected)
        {
            var calculator = new Sin();
            var actualResult = calculator.Calculate(firstValue);
            Assert.AreEqual(expected, actualResult);
        }
    }
}
