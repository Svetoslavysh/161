﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculat.OneArgument
{
    /// <summary>
    /// To calculate the cosine in degrees
    /// </summary>
    public class CosG : IOneArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="firstArgument">Operated number</param>
        /// <returns>Return the cosine in degrees of the argument</returns>
        public double Calculate(double firstArgument)
        {
            return Math.Cos(firstArgument * Math.PI / 180);
        }
    }
}
