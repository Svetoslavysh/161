﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Calculat.OneArgument;
using Calculat.TwoArgument;

namespace Calculat
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void computation(object sender, EventArgs e)
        {
            try
            {
                double answer1;
                double firstArgument = Convert.ToDouble(firstLine.Text);
                double secondArgument = Convert.ToDouble(secondLine.Text);
                ITwoArgument calculator = Factory.CreateCalculation(((Button)sender).Name);
                answer1 = calculator.Calculate(firstArgument, secondArgument);
                answer.Text = Convert.ToString(answer1);
            }


            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }

        private void operationOneArgument(object sender, EventArgs e)
        {
            try
            {
                double answer1;
                double argument = Convert.ToDouble(firstLine.Text);
                IOneArgument calculator = FactoryOneArgument.CreateCalculation(((Button)sender).Name);
                answer1 = calculator.Calculate(argument);
                answer.Text = Convert.ToString(answer1);
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }


    }
}
