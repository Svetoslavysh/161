﻿using System;

namespace Calculat_Elking.OneArgument
{
    /// <summary>
    /// Calculating the two in the power of the argument
    /// </summary>
    public class PowerOfTwo : IOperationForOneArgument
    {
        /// <summary>
        /// To implement a class problem
        /// </summary>
        /// <param name="Argument">Operated number</param>
        /// <returns>Returning the two to the power of the argument</returns>
        public double Calculate(double Argument)
        {
            return Math.Pow(2, Argument);
        }
    }
}
